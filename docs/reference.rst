Complete Reference
==================

pyfde
-----

.. automodule:: pyfde
    :members:
    :special-members: __call__, __iter__
    :show-inheritance:
    :inherited-members:

pyfde.bench
-----------
    
.. automodule:: pyfde.bench
    :members:
