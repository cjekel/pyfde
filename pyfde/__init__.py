from pyfde.classicde import ClassicDE
from pyfde.jade import JADE

__all__ = ['ClassicDE', 'JADE']
__version__ = '1.2.1'
